(function(){
  'use strict';
  var h = 600;
  var w = 960;

  // manually set up offset
  var offset_h = 340;
  var offset_w = 150;
  var colorscale = colorbrewer.YlOrBr[6];
  var svg = d3.select("#visualize-map")
              .append("svg")
              .attr("width", w )
              .attr("height", h);
  var quantize = d3.scale.quantize()
              .domain([0,50000]) // need to change based on data
              .range(colorscale)
  var initial_year = 2008;
  var legendBarWidth = 30,
      legendBarHeight = 10,
      legendOffsetX = 5,
      legendOffsetY = h-70;
  var data_map, data_csv, kcmt_fix;

  // create legend
  function create_legend() {
    svg.append('g')
       .attr('class', 'legend');

    svg.select('.legend').selectAll('.legend')
       .data(colorscale)
       .enter()
       .append('rect')
         .attr('x', legendOffsetX)
         .attr('y', function(d,i) {
           return (legendOffsetY + legendBarHeight*i);
         })
       .attr('width', legendBarWidth)
       .attr('height', legendBarHeight)
       .attr('fill', function(d){ return d; });

     // draw the legend text labels
     svg.select('.legend').selectAll('.legend')
       .data(colorscale)
       .enter()
         .append('text')
           .text(function(d,i){
             var quantizedRange = quantize.invertExtent(d);
             return Math.round(quantizedRange[0]) + ' - ' +
             Math.round(quantizedRange[1]) + ' t';
           })
           .attr('x', legendOffsetX+(legendBarWidth*1.25))
           .attr('y', function(d,i){
             return legendOffsetY + (legendBarHeight*0.9) + legendBarHeight*i;
           })
           .attr("font-family", "sans-serif")
           .attr("font-size", "10px")
           .attr("fill", "black");
   }

   function check_global(){
     return data_map && kcmt_fix && data_csv;
   }

   var change_transition = function(year){
     if (!check_global()) throw console.error("global variable is undefined");

    svg.selectAll("path").transition().delay(500).duration(1000)
      .attr('fill',function(d){
        return calculate_color(d, year);
      });
    svg.select('#year').selectAll('text').transition().delay(500).text(year);
    initial_year = year;
   }

   /**
    * menghitung warna yang cocok untu ditampilkan
    * @param {json} data
    * @param {string} kecamatan  - kecamatan yang berasal dari data csv
    * @param {string} peta_kecamatan - kecamtan yang berasal dari peta
    * @return {string} color value yang sesuai bedasarkan colorscale
   **/
   function calculate_color(d,year){
     if (!check_global()) throw console.error("global variable is undefined");

     var data = data_csv;
     var kecamatan_fix = kcmt_fix;
     var kecamatan = "",
         peta_kecamatan = "",
         value = 0;

     // fill the color based on time
     for (var i=0; i< data.length; i++){
       peta_kecamatan = d.properties.KECAMATAN.toLowerCase()
       kecamatan = data[i]["kecamatan"].toLowerCase();
       if (kecamatan == peta_kecamatan){
         for (var j=0; j < data[i]["time"].length; j++ ){
           if (data[i]["time"][j][year]){
             return quantize(data[i]["time"][j][year]);
           }
         }
       } else if (peta_kecamatan == kecamatan_fix[kecamatan]){
         for (var j=0; j < data[i]["time"].length; j++ ){
           if (data[i]["time"][j][year]){
             value += parseInt(data[i]["time"][j][year]);
           }
         }
       }
     }
     if (value > 0) return quantize(value);
     // display red color if no data
     return "#CF000F";
   }

  queue()
    .defer(d3.json, "bandung_kota.json")
    .defer(d3.json,"ref_kecamatan.json")
    .defer(d3.csv,"sample.csv",function(d){
      var temp={"kecamatan":"","time":[]};
      for (var key in d){
        if (key == "kecamatan"){
          temp[key]=d[key]
        } else if (key != 'no') {
          var val = {}
          val[key] = d[key]
          temp['time'].push(val);
        }
      }
      return temp
    })
    .await(ready);

  function ready(error, map, kecamatan_fix, data){
    if(error) throw error;

    data_map = map;
    data_csv = data;
    kcmt_fix = kecamatan_fix;

    // guess projection
    var center = d3.geo.centroid(map)
    var scale  = 150;
    var offset = [w/2, h/2];
    var projection = d3.geo.mercator().scale(scale).center(center)
        .translate(offset);

    // create path
    var path = d3.geo.path().projection(projection);

    // create map
    var bounds  = path.bounds(map);
    var hscale  = scale*w  / (bounds[1][0] - bounds[0][0]);
    var vscale  = scale*h / (bounds[1][1] - bounds[0][1]);
    var scale   = Math.round(((hscale < vscale) ? hscale : vscale) * 0.95 );
    var offset  = [ w - (bounds[0][0] + bounds[1][0])/2 + offset_w,
                    h - (bounds[0][1] + bounds[1][1])/2 + offset_h];

    projection = d3.geo.mercator().center(center)
                .scale(scale).translate(offset);
    path = path.projection(projection);

    svg.selectAll("path")
        .data(map.features)
      .enter().append("path")
        .attr("d", path)
        .attr("id",function(d){
          return mylib.slugify(d.properties.KECAMATAN.toLowerCase());
        })
        .attr("fill", function(d){
          return calculate_color(d,initial_year);
        })
        .attr('stroke','#bdc3c7')
        .on('mousemove',function(d){
          var data = data_csv;
          var kecamatan_fix = kcmt_fix;
          var kecamatan = "",
              peta_kecamatan = "",
              kecamatan_print="",
              value = 0;
          var text=""

          // loop data from csv
          for (var i=0; i< data.length; i++){
            peta_kecamatan = d.properties.KECAMATAN.toLowerCase()
            kecamatan = data[i]["kecamatan"].toLowerCase();

            if (kecamatan == peta_kecamatan){
              for (var j=0; j < data[i]["time"].length; j++ ){
                if (data[i]["time"][j][initial_year]){
                  value = data[i]["time"][j][initial_year];
                  kecamatan_print = kecamatan;
                  break;
                }
              }
              break;
            } else if (peta_kecamatan == kecamatan_fix[kecamatan]){
              for (var j=0; j < data[i]["time"].length; j++ ){
                if (data[i]["time"][j][initial_year]){
                  kecamatan_print += kecamatan + ",";
                  value += parseInt(data[i]["time"][j][initial_year]) + 0 ;
                  break;
                }
              }
            }
          }

        //change tooltip
        text = "<b>kec. "+kecamatan_print+"</b>"+" "+value+" ton";
        tooltip.html('<span><span class="glyphicon glyphicon-info-sign"></span> '+text+'</span>')
              .style("left", (d3.event.pageX) + "px")
              .style("top", (d3.event.pageY - 35) + "px");
        })
        .on('mouseover',function(d){
            tooltip.transition()
                .duration(500)
                .style("opacity", 1);

        })
        .on('mouseout',function(d){
          tooltip.style("opacity",0);
        });

    svg.append('g').attr('id','year').append('text').text(initial_year)
      .attr('x',w-100)
      .attr('y',41)
      .attr('font-family','sans-serif')
      .attr('font-size','35px')
      .attr('fill','black');

    create_legend();

    // create tooltip
    // Define the div for the tooltip
    var tooltip = d3.select("body").append("div")
          .attr("class", "tooltip_bro")
          .style("opacity", 1e-6);
  }

  // global function
  app.change_transition = change_transition;
})();
